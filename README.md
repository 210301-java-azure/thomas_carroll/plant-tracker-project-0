# Project0-plant-tracker

This is a simple jdbc web server that allows users to login and add and view plants to track from an azure postgres database. 

# Plant Tracker!

## Available Paths
---
### ***/signup***
##### Signup will take a body as a request and create a new record in the database with the given username and password.
##### Provides a new webtoken for the session
**Valid Requests**
* **POST** 

  * body
``` 
  {
    "username": "NewUsername", 
    "password": "NewPassword"
  } 
```
### ***/login***
##### Login will take a body as a request and check the database with the given username and password. 
##### Provides a new webtoken for the session
**Valid Requests**
* **POST** 
  * body
``` 
  {
    "username": "ExistingUsername", 
    "password": "ExistingPassword"
  } 
```
### ***/users***
##### Users provides a Json of all the users in the database. 
##### Requires a valid webtoken in order to view
**Valid Requests**
* **GET** 

### ***/users/:UserId***
##### Userid path paramater shows, deletes, or updates records in the user table
##### Requires a valid webtoken in order to view or use
**Valid Requests**
* **GET**
* **PUT**
  * body
``` 
  {
    "username": "NewUsername", 
    "password": "NewPassword"
  } 
```
* **DELETE**

### ***/plants***
##### Plants provides a Json of all the plants in the database or lets you add a new one
##### Requires a valid webtoken in order to view or use
**Valid Requests**
* **GET**
* **POST**
  * body
```
  {
        "plantName": "Croton",
        "plantScienceName": "Codiaeum variegatum",
        "lastWatered": "03/18/2021", --(mm/dd/yyyy)
        "lastFed": "02/03/2021",     --(mm/dd/yyyy)
        "plantHeight": 8.5,
        "plantWidth": 8.99,
        "plantOwnerId": 1
 }
```

### ***/plants/:UserId***
##### UserId path paramater shows all records in the plants table owned by a particular user
##### Requires a valid webtoken in order to view or use
**Valid Requests**
* **GET**

### ***/plants/:PlantId***
##### PlantId path paramater udpates or deletes a in the plants table
##### Requires a valid webtoken in order to view or use
**Valid Requests**
* **PUT**
  * body 
```
  {
        "lastWatered": "03/18/2021", --(mm/dd/yyyy)
        "lastFed": "02/03/2021",     --(mm/dd/yyyy)
        "plantHeight": 8.5,
        "plantWidth": 8.99,
 }
```

* **DELETE**

---
---

# Testing

## Integration
  Runs a test of the authentication capabilities and a brief check of the paths to make sure everything is functioning. 
