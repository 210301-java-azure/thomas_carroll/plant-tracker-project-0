package com.planttrack.controllers;

import com.planttrack.models.PlantItem;
import com.planttrack.models.UserItem;
import com.planttrack.service.PlantService;
import com.planttrack.service.PlantServiceImpl;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PlantsController {

    Logger logger = LoggerFactory.getLogger(PlantsController.class);
    PlantService service = new PlantServiceImpl();


    public void handleGetAllPlants(Context context) {
        context.json(service.getAllPlants());
    }

    public void handleGetPlantByUserId(Context context) {
        logger.info("handling get plant by user id!");
        String idString = context.pathParam("plantOwnerId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            context.json(service.getPlantByUserId(idInput));
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

    public void handleDeletePlantByPlantId(Context context) {
        logger.info("handling delete plant by plant id!");
        String idString = context.pathParam("plantOwnerId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.deletePlantById(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleAddNewPlant(Context context) {
        logger.info("handling post new plant!");
        PlantItem item = context.bodyAsClass(PlantItem.class);
        logger.info("adding new item: {}", item);
        PlantItem newItem = service.addNewPlant(item);
        context.status(201);
        context.json(newItem);
    }


    public void handleChangeLastFedAndWatered(Context context) {
        PlantItem plant = context.bodyAsClass(PlantItem.class);
        String plantId = context.pathParam("plantOwnerId");
        if(plantId.equals("^\\d+$")){
            int idInput = Integer.parseInt(plantId);
            service.updatePlantFedWatered(plant, idInput);
            logger.info("plants lastfed and lastwatered successfully updated");
            context.status(200);
        } else {
            context.status(400);
            throw new BadRequestResponse("input \""+ plantId +"\" cannot be parsed to an int");
        }
    }
}

