package com.planttrack.controllers;


import com.planttrack.managers.TokenManager;
import com.planttrack.managers.TokenManagerImpl;
import com.planttrack.models.UserItem;
import com.planttrack.service.UserService;
import com.planttrack.service.UserServiceImpl;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UserController{
    public UserController(){super();}
    Logger logger = LoggerFactory.getLogger(UserController.class);
    UserService service = new UserServiceImpl();
    TokenManager tokenManager = new TokenManagerImpl();

    public void handleGetAllUsers(Context context){
        context.json(service.getAllUsers()); // json method converts object to JSON
        context.status(200);
    }

    public void deleteUserById(Context context){
        String idString = context.pathParam("userId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting user with id: " + idInput);
            service.deleteUserById(idInput);
            context.status(200);
        } else {
            context.status(400);
            throw new BadRequestResponse("input \"" + idString + "\" cannot be parsed to an int");
        }
    }

    public void handleGetUserById(Context context) {
        String idString = context.pathParam("userId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            context.status(200);
            context.json(service.getUserById(idInput));
        } else {
            context.status(400);
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void userSignup(Context context){
        UserItem newUser = context.bodyAsClass(UserItem.class);
        service.addNewUser(newUser);
        String newToken = tokenManager.issueToken(newUser.getUsername(), newUser.getPassword());
        context.header("Authorization", newToken);
        context.status(200);
        context.json(newToken);
    }

    public void userLogin (Context context){
        UserItem user = context.bodyAsClass(UserItem.class);
        if(service.userLogin(user)) {
            String newToken = tokenManager.issueToken(user.getUsername(), user.getPassword());
            context.header("Authorization", newToken);
            context.status(200);
            context.json(newToken);
        } else {
            context.status(401);
        }
    }

    public void authorizeToken(Context context) {
        logger.info("attempting to authorize token");
        String authHeader = context.header("Authorization");
        if(authHeader!=null && tokenManager.authorizeToken(authHeader)){
            logger.info("request is authorized, proceeding to handler method");
            context.status(200);
        } else {
            logger.warn("improper authorization");
            context.status(401);
            throw new UnauthorizedResponse();
        }
    }

    public void handlePutNewUsernamePassword(Context context) {
        UserItem user = context.bodyAsClass(UserItem.class);
        String userId = context.pathParam("userId");
        if(userId.matches("^\\d+$")){
            int idInput = Integer.parseInt(userId);
            service.updateUser(user, idInput);
            String newToken = tokenManager.issueToken(user.getUsername(), user.getPassword());
            context.header("Authorization", newToken);
            context.status(200);
            context.json(newToken);
        } else {
            context.status(400);
            throw new BadRequestResponse("input \""+ userId +"\" cannot be parsed to an int");
        }
    }
}


