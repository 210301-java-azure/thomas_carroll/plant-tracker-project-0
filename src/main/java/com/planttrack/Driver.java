package com.planttrack;


public class Driver {

    public static void main(String[] args) {
        //Starts up javalin web server and instantiates our AuthController object which has methods for checking auths
        JavalinApp javalinApp = new JavalinApp();
        javalinApp.start(7000);
    }
}

