package com.planttrack.service;

import com.planttrack.models.UserItem;


import java.util.List;

public interface UserService {
    UserItem addNewUser(UserItem newUser);

    Boolean userLogin(UserItem request);

    List<UserItem> getAllUsers();

    void deleteUserById(int userId);

    UserItem getUserById(int idInput);

    Boolean authorizeToken(String password, String username);

    void updateUser(UserItem user, int idString);
}
