package com.planttrack.service;

import com.planttrack.dao.UsersDAOimpl;
import com.planttrack.managers.TokenManager;
import com.planttrack.managers.TokenManagerImpl;
import com.planttrack.models.UserItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class UserServiceImpl implements UserService {
    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    UsersDAOimpl usersDAO = new UsersDAOimpl();

    @Override
    public UserItem addNewUser(UserItem newUser) {
        return usersDAO.addNewUser(newUser);
    }


    @Override
    public Boolean userLogin(UserItem userItem) {
        String username = userItem.getUsername();
        String password = userItem.getPassword();
        String dbPassword = usersDAO.getPasswordByName(username);
        if (password.equals(dbPassword)) {
            logger.info("password check returned true");
            return true;
        } else {
            logger.info("password check returned false");
            return false;
        }
    }

    @Override
    public List<UserItem> getAllUsers() {
        return usersDAO.getAllUsers();
    }

    @Override
    public void deleteUserById(int userId) {
        usersDAO.deleteUserById(userId);
    }

    @Override
    public UserItem getUserById(int userId) {return usersDAO.getUserById(userId);}

    @Override
    public Boolean authorizeToken(String password, String username) {
        String dbPassword = usersDAO.getPasswordByName(username);
        if (password.equals(dbPassword)) {
            logger.info("password check returned true");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void updateUser(UserItem user, int userId) {
        usersDAO.updateUserById(user, userId);
    }
}
