package com.planttrack.service;

import com.planttrack.dao.PlantsDAO;
import com.planttrack.dao.PlantsDAOimpl;
import com.planttrack.models.PlantItem;


import java.util.List;

public class PlantServiceImpl implements PlantService{
    PlantsDAO plantsDAO = new PlantsDAOimpl();

    @Override
    public PlantItem addNewPlant(PlantItem newPlant) {
        return plantsDAO.addNewPlant(newPlant);
    }

    @Override
    public List<PlantItem> getAllPlants() {
        return plantsDAO.getAllPlants();
    }

    @Override
    public void deletePlantById(int plantId) {
        plantsDAO.deletePlantById(plantId);
    }

    @Override
    public List<PlantItem> getPlantByUserId(int plantId) {return plantsDAO.getPlantsByUserId(plantId);}

    @Override
    public List<PlantItem> getPlantsByHeight(){return plantsDAO.getPlantsByHeight();}

    @Override
    public PlantItem updatePlantFedWatered(PlantItem plant, int idInput) {
        return plantsDAO.updatePlantById(plant, idInput);
    }
}
