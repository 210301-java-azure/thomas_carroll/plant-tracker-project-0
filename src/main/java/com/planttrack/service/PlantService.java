package com.planttrack.service;

import com.planttrack.controllers.PlantsController;
import com.planttrack.models.PlantItem;


import java.util.List;

public interface PlantService {
    PlantItem addNewPlant(PlantItem newPlant);

    List<PlantItem> getAllPlants();

    void deletePlantById(int plantId);

    List<PlantItem> getPlantByUserId(int userId);

    List<PlantItem> getPlantsByHeight();

    PlantItem updatePlantFedWatered(PlantItem plant, int idInput);
}
