package com.planttrack.models;

import org.eclipse.jetty.server.Authentication;

import java.io.Serializable;

public class UserItem implements Serializable {
    private int userId;
    private String username;
    private String password;

    public UserItem(){
        super();
    }

    public UserItem(int userId, String username, String password){
        this.userId = userId;
        this.username = username;
        this.password = password;
    }

    public UserItem(int userId, String username) {
        this.userId = userId;
        this.username = username;
        //this does not show passwords when displaying!
    }

    public UserItem(String username, String password){
        this.username = username;
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
