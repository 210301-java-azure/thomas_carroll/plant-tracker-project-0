package com.planttrack.models;

import java.sql.Date;

public class PlantItem {
    private int plantId;
    private String plantName;
    private String plantScienceName;
    private String lastWatered;
    private String lastFed;
    private float plantHeight;
    private float plantWidth;
    private int userId;


    public PlantItem(String test_plant, String testius_plantius, String s, String s1, double v, double v1, int i){
        super();
    }
    public PlantItem(String lastWatered, String lastFed){
        this.lastWatered = lastWatered;
        this.lastFed = lastFed;
    }

    public PlantItem(int plantId,
                     String plantName,
                     String plantScienceName,
                     String lastWatered,
                     String lastFed,
                     float plantHeight,
                     float plantWidth,
                     int plantOwnerId
                    ) {
        this.plantId = plantId;
        this.plantName = plantName;
        this.plantScienceName = plantScienceName;
        this.lastWatered = lastWatered;
        this.lastFed = lastFed;
        this.plantHeight = plantHeight;
        this.plantWidth = plantWidth;
        this.userId = plantOwnerId;
    }

    public int getPlantId() {
        return plantId;
    }

    public void setPlantId(int plantId) {
        this.plantId = plantId;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getPlantScienceName() {
        return plantScienceName;
    }

    public void setPlantScienceName(String plantScienceName) {
        this.plantScienceName = plantScienceName;
    }

    public String getLastWatered() { return lastWatered; }

    public void setLastWatered(String lastWatered) { this.lastWatered = lastWatered; }

    public String getLastFed() { return lastFed; }

    public void setLastFed(String lastFed) {
        this.lastFed = lastFed;
    }

    public float getPlantHeight() {
        return plantHeight;
    }

    public void setPlantHeight(float plantHeight) {
        this.plantHeight = plantHeight;
    }

    public float getPlantWidth() {
        return plantWidth;
    }

    public void setPlantWidth(float plantWidth) {
        this.plantWidth = plantWidth;
    }

    public int getPlantOwnerId() {
        return userId;
    }

    public void setPlantOwnerId(int userId) {
        this.userId = userId;
    }
}
