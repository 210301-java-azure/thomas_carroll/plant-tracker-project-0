package com.planttrack.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionTest {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(ConnectionTest.class);

        try {
            Connection c = ConnectionUtil.getConnection();
            logger.info(c.getMetaData().getDriverName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
