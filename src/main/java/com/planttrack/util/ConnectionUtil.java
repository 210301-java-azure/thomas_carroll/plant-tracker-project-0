package com.planttrack.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static Connection connection;

    private ConnectionUtil(){
        super();
    }

    public static Connection getConnection() throws SQLException {
        if(connection == null || connection.isClosed()) {
            String connectionUrl = System.getenv("connectionUrl");
            // create a connection
            connection = DriverManager.getConnection(connectionUrl);
        }
        return connection;

    }
}
