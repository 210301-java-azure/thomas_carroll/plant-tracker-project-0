package com.planttrack.dao;

import com.planttrack.models.PlantItem;

import java.util.List;

public interface PlantsDAO {
    public List<PlantItem> getAllPlants();
    public PlantItem addNewPlant(PlantItem newPlant);
    public List<PlantItem> getPlantsByUserId(int userId);
    public PlantItem updatePlantById(PlantItem updatedPlant, int plantId);
    public void deletePlantById(int plantId);
    public List<PlantItem> getPlantsByHeight();
}
