package com.planttrack.dao;

import com.planttrack.models.UserItem;
import com.planttrack.service.UserService;

import java.util.List;

public interface UsersDAO {
    public List<UserItem> getAllUsers();
    public UserItem addNewUser(UserItem newUser);
    public UserItem getUserById(int userId);
    public UserItem updateUserById(UserItem updatedUser, int userId);
    public void deleteUserById(int userId);
    public String getPasswordByName(String username);

}
