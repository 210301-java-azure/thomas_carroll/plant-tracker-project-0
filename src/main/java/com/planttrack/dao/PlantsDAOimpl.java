package com.planttrack.dao;

import com.planttrack.models.PlantItem;
import com.planttrack.models.UserItem;
import com.planttrack.util.ConnectionUtil;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
//TODO Fill this in


public class PlantsDAOimpl implements PlantsDAO{
    Logger logger = LoggerFactory.getLogger(PlantsDAOimpl.class);

    @Override
    public List<PlantItem> getAllPlants() {
        List<PlantItem> items = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user_plants");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int plantId = resultSet.getInt("plantId");
                String plantName = resultSet.getString("plantName");
                String plantScienceName = resultSet.getString("plantScienceName");
                String lastWatered = resultSet.getString("lastwatered");
                String lastFed = resultSet.getString("lastfed");
                Float plantHeight = resultSet.getFloat("plantHeight");
                Float plantWidth = resultSet.getFloat("plantWidth");
                int userId = resultSet.getInt("usersId");
                PlantItem item = new PlantItem(
                        plantId,
                        plantName,
                        plantScienceName,
                        lastWatered,
                        lastFed,
                        plantHeight,
                        plantWidth,
                        userId
                );
                items.add(item);
            }
            logger.info("selecting all items from db - "+ items.size()+ " items retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return items;
    }

    @Override
    public PlantItem addNewPlant(PlantItem newPlant) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement(
                    "insert into user_plants values (default, ?, ?, ?, ?, ?, ?, (select userid from users where userId=?))"
            );
            prepareStatement.setString(1, newPlant.getPlantName());
            prepareStatement.setString(2, newPlant.getPlantScienceName());
            prepareStatement.setString(3, newPlant.getLastWatered());
            prepareStatement.setString(4,newPlant.getLastFed());
            prepareStatement.setFloat(5,newPlant.getPlantHeight());
            prepareStatement.setFloat(6,newPlant.getPlantWidth());
            prepareStatement.setInt(7,newPlant.getPlantOwnerId());
            prepareStatement.executeUpdate();
            logger.info("Successfully added " + newPlant.getPlantName() + " to database with id: " + newPlant.getPlantId());
            return newPlant;
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return newPlant;
    }

    @Override
    public List<PlantItem> getPlantsByUserId(int userId) {
        List<PlantItem> items = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from user_plants where usersId = ?");
            prepareStatement.setInt(1,userId);
            ResultSet resultSet = prepareStatement.executeQuery();

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int plantId = resultSet.getInt("plantId");
                String plantName = resultSet.getString("plantName");
                String plantScienceName = resultSet.getString("plantScienceName");
                String lastWatered = resultSet.getString("lastwatered");
                String lastFed = resultSet.getString("lastfed");
                Float plantHeight = resultSet.getFloat("plantHeight");
                Float plantWidth = resultSet.getFloat("plantWidth");
                int usersId = resultSet.getInt("usersId");
                PlantItem item = new PlantItem(
                        plantId,
                        plantName,
                        plantScienceName,
                        lastWatered,
                        lastFed,
                        plantHeight,
                        plantWidth,
                        usersId
                );
                items.add(item);
            }
            return items;
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }


    @Override
    public PlantItem updatePlantById(PlantItem updatedPlant, int plantId) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("update user_plants set lastwatered = ?,  last fed = ? where plantid = ?");
            prepareStatement.setString(1, updatedPlant.getLastWatered());
            prepareStatement.setString(2, updatedPlant.getLastFed());
            prepareStatement.setInt(3, updatedPlant.getPlantId());
            prepareStatement.executeUpdate();
            logger.info("Successfully updated " + updatedPlant.getPlantName());
            return updatedPlant;
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return updatedPlant;

    }


    @Override
    public void deletePlantById(int plantId) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("Delete from user_plants where plantid = ?");
            prepareStatement.setInt(1,plantId);
            prepareStatement.executeQuery();
            logger.info("Userid: " + plantId + " Has been removed");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
    }

    @Override
    public List<PlantItem> getPlantsByHeight() {
        return null;
    }
}
