package com.planttrack.dao;

import com.planttrack.models.UserItem;
import com.planttrack.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UsersDAOimpl implements UsersDAO{
    Logger logger = LoggerFactory.getLogger(UsersDAOimpl.class);

    @Override
    public List<UserItem> getAllUsers() {
        List<UserItem> items = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from users");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int userId = resultSet.getInt("userId");
                String username = resultSet.getString("username");
                UserItem item = new UserItem(userId, username, "Hidden");
                items.add(item);
            }
            logger.info("selecting all items from db - "+ items.size()+ " items retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return items;
    }

    @Override
    public UserItem addNewUser(UserItem newUser) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("insert into users (username, password) values (?, ?)");
            prepareStatement.setString(1, newUser.getUsername());
            prepareStatement.setString(2, newUser.getPassword());
            prepareStatement.executeUpdate();
            logger.info("Successfully added " + newUser.getUsername() + " to database");
            return newUser;
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return newUser;

    }



    @Override
    public UserItem getUserById(int userId) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from users where userId = ?");
            prepareStatement.setInt(1,userId);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String username = resultSet.getString("username");
                logger.info("1 item retrieved from database by id: " + userId);
                return new UserItem(userId, username, "Test");
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;

    }
    @Override
    public String getPasswordByName(String username) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select password from users where username = ?");
            prepareStatement.setString(1,username);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String password = resultSet.getString("password");
                logger.info("password retrieved to be compared");
                return password;
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;

    }

    @Override
    public UserItem updateUserById(UserItem updatedUser, int userId) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("update users set username = ?, password = ? where userid = ?");
            prepareStatement.setString(1, updatedUser.getUsername());
            prepareStatement.setString(2, updatedUser.getPassword());
            prepareStatement.setInt(3, updatedUser.getUserId());
            prepareStatement.executeUpdate();
            logger.info("Successfully updated " + updatedUser.getUsername() + " to database");
            return updatedUser;
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return updatedUser;

    }

    @Override
    public void deleteUserById(int userId) {
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from users where userId = ?")){
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();
            logger.info("deleted item with id: {}", userId);
        } catch (SQLException e) {
            logException(e);
        }
    }
    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }

}
