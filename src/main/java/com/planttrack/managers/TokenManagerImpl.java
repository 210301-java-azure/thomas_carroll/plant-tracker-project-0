package com.planttrack.managers;

import com.planttrack.service.UserService;
import com.planttrack.service.UserServiceImpl;
import io.javalin.http.ForbiddenResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;

public class TokenManagerImpl implements TokenManager {
    Logger logger = LoggerFactory.getLogger(TokenManagerImpl.class);
    UserService user = new UserServiceImpl();
    private final Key key;

    public TokenManagerImpl() {
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    @Override
    public String issueToken(String userName, String password) {
        String token = Jwts.builder().setAudience(userName).setSubject(password).signWith(key).compact();
        logger.info("token issued");
        return token;
    }
    @Override
    public boolean authorizeToken(String token) {
        try {
            String subjectPassword = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
            String subjectUser = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getAudience();
            if(user.authorizeToken(subjectPassword, subjectUser)){
                logger.info("token authorized");
                return true;
            } else {
                logger.info("token not recognized");
                return false;
            }

        } catch (Exception ex){
            throw new ForbiddenResponse();
        }
    }
}
