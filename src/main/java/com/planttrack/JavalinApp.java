package com.planttrack;

import com.planttrack.controllers.PlantsController;
import com.planttrack.controllers.UserController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    UserController userController = new UserController();
    PlantsController plantsController = new PlantsController();

        Javalin app = Javalin.create().routes(() -> {
        //---------------------------SIGNUP------------------------------------------
        path("/signup", () -> post(userController::userSignup));
        //---------------------------LOGINS-----------------------------------------
        path("/login", () -> post(userController::userLogin));
        //---------------------------USERS-----------------------------------------
        path("/users", () -> {
            before("/", userController::authorizeToken);
            before("/*", userController::authorizeToken);
            get(userController::handleGetAllUsers);
            path(":userId", () -> {
                get(userController::handleGetUserById);
                delete(userController::deleteUserById);
                post(userController::handlePutNewUsernamePassword);
            });
        });
        //---------------------------USERS-----------------------------------------
        path("/plants", () -> {
            before("/", userController::authorizeToken);
            before("/*", userController::authorizeToken);
            get(plantsController::handleGetAllPlants);
            post(plantsController::handleAddNewPlant);

            path(":plantOwnerId", () -> {
                post(plantsController::handleChangeLastFedAndWatered);
                get(plantsController::handleGetPlantByUserId);
                delete(plantsController::handleDeletePlantByPlantId);
            });
        });
    });
        public void start(int port){this.app.start(port);}

        public void stop() {this.app.stop();}
}

