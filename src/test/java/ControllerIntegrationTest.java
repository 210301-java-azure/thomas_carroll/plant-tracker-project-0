import com.planttrack.JavalinApp;
import com.planttrack.controllers.PlantsController;
import com.planttrack.models.PlantItem;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class ControllerIntegrationTest {

    Logger logger = LoggerFactory.getLogger(PlantsController.class);

    private static JavalinApp app = new JavalinApp();

    @BeforeAll
    public static void startService(){
        app.start(7000);
    }

    @AfterAll
    public static void stopService(){
        app.stop();
    }

    @Test
    public void testLoginUserUnsuccessful(){
        HttpResponse<String> response = Unirest.post("http://localhost:7000/login")
                .body("{\"username\": \"wronguser\",\"password\": \"wrongpassword\"}")
                .asString();
        logger.info("JWT not received");
        assertAll(
                ()->assertEquals(401, response.getStatus()));
    }


    @Test
    public void testGetAllUsersAuthenticated(){
        HttpResponse<String> jwt = Unirest.post("http://localhost:7000/login")
                .body("{\"username\": \"firstuser\",\"password\": \"firstpassword\"}")
                .asString();

        HttpResponse<List<PlantItem>> response= Unirest.get("http://localhost:7000/users")
                .header("Authorization", jwt.getHeaders().getFirst("Authorization"))
                .asObject(new GenericType<List<PlantItem>>(){});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }
    @Test
    public void testGetAllPlantsAuthenticated(){
        HttpResponse<String> jwt = Unirest.post("http://localhost:7000/login")
                .body("{\"username\": \"firstuser\",\"password\": \"firstpassword\"}")
                .asString();

        HttpResponse<List<PlantItem>> response= Unirest.get("http://localhost:7000/users")
                .header("Authorization", jwt.getHeaders().getFirst("Authorization"))
                .asObject(new GenericType<List<PlantItem>>(){});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }
    @Test
    public void testGetUserById(){
        HttpResponse<String> jwt = Unirest.post("http://localhost:7000/login")
                .body("{\"username\": \"firstuser\",\"password\": \"firstpassword\"}")
                .asString();

        HttpResponse<PlantItem> response= Unirest.get("http://localhost:7000/users/1")
                .header("Authorization", jwt.getHeaders().getFirst("Authorization"))
                .asObject(PlantItem.class);
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().toString() != null)
        );
    }


}




