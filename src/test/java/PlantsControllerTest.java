import com.planttrack.controllers.PlantsController;
import com.planttrack.models.PlantItem;
import com.planttrack.service.PlantService;
import com.planttrack.service.PlantServiceImpl;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class PlantsControllerTest {

    @InjectMocks
    private PlantsController plantsController;

    /*
    We created a mock service here, a dummy object which we can give behavior by stubbing its methods
    We could also create a spy, which is a real object whose methods can be stubbed and invoked instead of the real methods
     */
    @Mock
    private PlantService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllItemsHandler(){
        Context context = mock(Context.class);

        List<PlantItem> plants = new ArrayList<>();
        plants.add(new PlantItem("Test Plant", "Testius Plantius", "01/01/1970", "01/24/1997", 10.54, 6.54, 1));
        plants.add(new PlantItem("Rev Plant", "Revius Plantius", "01/01/1970", "01/24/1997", 14, 3.25, 1));
        plants.add(new PlantItem("Mockito Plant", "Mockius Plantius", "01/01/1970", "01/24/1997", 1.45, 1.02, 2));

        when(service.getAllPlants()).thenReturn(plants);
        plantsController.handleGetAllPlants(context);
        verify(context).json(plants);
    }
}
